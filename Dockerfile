FROM python:3.10
RUN mkdir /app
RUN apt-get update && apt-get install -y iputils-ping git
WORKDIR /app
ADD . /app/
RUN pip install -r requeriments.txt
EXPOSE 8000
CMD ["sh","run.sh"]